const currency = require("currency.js");

const FlightScraper = require('./scraper');
let flightData = require('./lib/data');
const config = require('./lib/config');
const asyncForEach = require('./utils/asyncForEach');

let departureCity = "";

const flightOrganizer = async (locations, maxPrice) => {
  try {
    const configs = config.getData();
    let locationArray = createLocationArray(configs.destinations);
    let flightObjectArray = locationArray.map(location => buildObj(location));

    await asyncForEach(flightObjectArray, async search => {
      await getFlights(search, configs.departureCity);
    });

    flightData.setData({
      flights: flightObjectArray,
      departureCity,
      date: new Date()
    });
    //FlightScraper.closeSession();
  } catch (error) {
    console.log("FO ERROR: ", err);
    process.exit();
  }
}

function createLocationArray(locations) {
  let arr = ['Local', 'Europe'];
  if (locations === '') {
    return arr;
  } else {
    let userInput = locations.split(",").map(item => item.trim());
    return [...arr, ...userInput];
  }
}

const buildObj = (location) => {
  return {
    name: location,
    flights: []
  };
}

const getFlights = async (search, departureCityInput) => {
  try {
    const $ = search.name !== 'Local' ?
      await FlightScraper.fetchLocationData(departureCityInput, search.name) :
      await FlightScraper.fetchLocalData(departureCityInput);
    if (departureCity === '')  departureCity = $('input[aria-label="Where from?"]').val();
    $("li.lPyEac").each((index, element) => extractData($(element), search.flights));
    search.flights.sort((a, b) => (currency(a.price) - currency(b.price)))
  } catch (error) {
    console.log("ERR0R", error);
    process.exit();
  }
}

const extractData = (parent, storage) => {
  let flightInfo = {};
  const configs = config.getData();
  flightInfo.destination = parent.find('h3').text();
  flightInfo.price = getPrice(parent.find('span').text());
  flightInfo.dates = parent.find('h3').next().text();
  if (flightInfo.price !== '') {
    if (Number.isNaN(configs.maxPrice)) {
      storage.push(flightInfo);
    } else if (parseInt(flightInfo.price.replace(/\D/g,'')) <= configs.maxPrice) {
      storage.push(flightInfo);
    }
  }
}

const getPrice = str => {
  const dollarSign = str.lastIndexOf('$');
  return str.slice(dollarSign);
}

module.exports = flightOrganizer;
