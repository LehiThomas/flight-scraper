## How to Run
Pull down project and run the following command from the project folder
```
node index.js
```

## Questions Guide
All questions can be bypassed (just press enter)
Question 1: Change Departure City: (Press Enter to keep current location)
Will use your computers location by default, or can be changed to search from a different location.
Question 2: Locations to add (comma separated):
Will search local area and Europe by default. If more destinations are desired, enter them here. Will work with States, Countries, Continents. WILL NOT WORK WITH CITIES.
Question 3: Max price (leave blank for no max):
Will only show destinations below the inputed value. If no value is given it will show all found destinations.
Question 4: Search frequency:
How frequently the program will run.


## How to Find Your Price
Go to https://www.google.com/travel/explore and use the correct departure and destination. Hit ENTER and you should see the shown price!