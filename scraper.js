const Nightmare = require('nightmare');
const cheerio = require("cheerio");

const nightmare = Nightmare({ show:false });
const siteUrl = 'https://www.google.com/travel/explore';

const fetchLocalData = async departureCity => {
  return await nightmare.goto(siteUrl)
    .insert('input[aria-label="Where from?"]', false)
    .insert('input[aria-label="Where from?"]', departureCity)
    .type('input[aria-label="Where from?"]', '\u000d')
    .wait('.s38TVc')
    .evaluate(()=> document.querySelector('body').innerHTML)
    .then(response => {
      return cheerio.load(response);
    }).catch(err => {
      console.log("REQUEST ERROR: ", err);
      process.exit();
    })
};

const fetchLocationData = async (departureCity, destination) => {
  return await nightmare.goto(siteUrl)
    .insert('input[aria-label="Where from?"]', false)
    .insert('input[aria-label="Where from?"]', departureCity)
    .type('input[aria-label="Where from?"]', '\u000d')
    .wait(1500)
    .type('input[aria-label="Where to?"]', destination)
    .type('input[aria-label="Where to?"]', '\u000d')
    .wait(1000)
    .wait('.s38TVc')
    .evaluate(()=> document.querySelector('body').innerHTML)
    .then(response => {
      return cheerio.load(response);
    }).catch(err => {
      console.log("REQUEST ERROR: ", err);
      process.exit();
    })
};

const closeSession = async () =>{
  return await nightmare.goto(siteUrl)
    .end()
    .then(() => {
      return
    }).catch(err => {
      console.log("REQUEST ERROR: ", err);
      process.exit();
    })
}

module.exports = {
  fetchLocalData,
  fetchLocationData,
  closeSession
}