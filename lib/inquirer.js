const inquirer = require('inquirer');

module.exports = {
  askForNewLocations: () => {
    const questions = [
      {
        name: 'departureCity',
        type: 'input',
        message: 'Change Departure City: (Press Enter to keep current location)'
      },
      {
        name: 'destinations',
        type: 'input',
        message: 'Locations to add (comma separated):'
      },
      {
        name: 'maxPrice',
        type: 'number',
        message: 'Max price (leave blank for no max):'
      },
      {
        name: 'frequency',
        type: 'list',
        message: 'Search frequency:',
        choices: [
          {
            name: 'Once'
          },
          {
            name: 'Every 12 hours'
          }
        ]
      }
    ];
    return inquirer.prompt(questions);
  },
};