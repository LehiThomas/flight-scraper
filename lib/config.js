let configData;

function setData(data){
  configData = data;
}

function getData(){
  return configData;
}

module.exports = {
  getData,
  setData
}

// { locationPrompt: '', maxPrice: NaN, frequency: 'Once' }