let flightData;

function setData(data){
  flightData = data;
}

function getData(){
  return flightData;
}

module.exports = {
  getData,
  setData
}