const clear = require('clear');
const chalk = require('chalk');
const figlet = require('figlet');
const boxen = require('boxen');
const ora = require('ora');

const inquirer  = require('./lib/inquirer');
var CronJob = require('cron').CronJob;

const flightOrganizer = require('./flights');
const flightData = require('./lib/data');
const config = require('./lib/config');

clear();

console.log(
  chalk.yellow(
    figlet.textSync('Flight Scraper', { horizontalLayout: 'full' })
  )
);

var job = new CronJob('0 */12 * * *', async function() {
  doTheThing();
}, null, true, 'America/Chicago');

function displayFlights(data) {
  let text = `Departure City: ${data.departureCity} \nTime received: ${data.date}`
  console.log(boxen(chalk.cyan(text), {padding: 1}));
  data.flights.forEach(search => {
    console.log(boxen(chalk.blue(search.name), {
      margin: {
        top: 1,
        bottom: 0,
        left: 10,
        right: 5
      },
      padding: {
        top: 1,
        bottom: 1,
        left: 5,
        right: 5
      },
      borderStyle: 'double'
    }));
    console.table(search.flights);
  });
};

async function doTheThing(){
  const spinner = ora(`${chalk.red('Retrieving flights...')}`);
  spinner.start();
  await flightOrganizer();
  spinner.stop();
  displayFlights(flightData.getData());
}

async function init(credentials) {
  config.setData(credentials);
  if (credentials.frequency === 'Once') {
    await doTheThing();
    process.exit();
  } else {
    doTheThing();
    job.start();
  }

}

const run = async () => {
  const credentials = await inquirer.askForNewLocations();
  await init(credentials);
};

run();

// clickable links?
// unique ID for entries?